# Network Routing using MWUA

This demo program uses multiplicative weights update algorithm to solve network routing. This is a final project for [evolutionary dynamics 2018 fall course](https://nol.ntu.edu.tw/nol/coursesearch/print_table.php?course_id=221%20U8300&class=&dpt_code=2010&ser_no=18853&semester=106-2&lang=EN) in National Taiwan University.

*Warning*: The demo program is not intended for production environment. If you are using this program for reseach or something, do it at your own risk.

## Simulation setting

* Suppose there are _n_ nodes. Nodes are indexed from 0 to n-1. (No node called n!)
* Node 0 is the sender, while node n-1 is the receiver.
* Node 0 sends a new ping message when there is no message in the network.
* In each _tick_, every message is delivered to a neighbor node.

## Build

Make sure you have the dependencies installed.

* python >= 3
* python-tabulate

You may run `pip` to install required dependencies.
```sh
sudo pip install -r requirements.txt
```

## Examples

* Simulate a line graph network of 5 nodes in 1000 ticks.
```sh
# usage: simulator.py N_NODES [EDGE [EDGE ...]] [--num-ticks NUM_TICKS]
./simulator.py 5 0,1 1,2 2,3 3,4 --num-ticks 1000
```

* Change learning rate and reward decay (discount) constant.
```sh
./simulator.py 5 0,1 1,2 2,3 3,4 --num-ticks 1000 --learning-rate 0.1 --reward-decay 0.8
```

* Store simulation logs to a file.
```sh
./simulator.py 5 0,1 1,2 2,3 3,4 --num-ticks 1000 --log-file /path/to/your/file.txt
```
