#!/usr/bin/env python3
import random
import argparse
import json
import signal
import itertools
import time
import pprint
from collections import defaultdict, deque

from tabulate import tabulate

# global constants
TYPE_PING = 'ping'
TYPE_PONG = 'pong'

PAYLOAD_SUCCESS = 'success'
PAYLOAD_FAIL    = 'fail'

# global termination indicator
TERMINATE = False


class Message:
    def __init__(self, msg_type, seq, src, dst, payload=None):
        assert msg_type in (TYPE_PING, TYPE_PONG)
        self.msg_type = msg_type
        self.seq = seq
        self.src = src
        self.dst = dst
        self.count = 0
        self.payload = payload


class Node:
    def __init__(self, name, neighbors):
        n_neighbors = len(neighbors)
        self.name = name
        self.neighbors = neighbors

        if n_neighbors > 0:
            default_weights = {neighbor: 1 / n_neighbors for neighbor in self.neighbors}
            default_rewards = {neighbor: None for neighbor in self.neighbors}

            self.weight_table = defaultdict(lambda: default_weights.copy())
            self.reward_table = defaultdict(lambda: default_rewards.copy())
        else:
            self.weight_table = None
            self.reward_table = None

        self.msg_queue = deque()
        self.ping_log = dict()
        self.seq_counter = defaultdict(int)

    def update_weights(self, node_id, chosen_neighbor, reward):
        assert abs(reward) <= 1.0

        weights = self.weight_table[node_id]
        rewards = self.reward_table[node_id]

        # update reward table
        if rewards[chosen_neighbor] is None:
            rewards[chosen_neighbor] = reward
        else:
            rewards[chosen_neighbor] = rewards[chosen_neighbor] * ARGS.reward_decay + reward

        reward_scale = 0.5 * (1 - ARGS.reward_decay)

        # update weights
        for neighbor in self.neighbors:
            if rewards[neighbor] is None:
                weights[neighbor] *= 1 - ARGS.learning_rate * reward_scale
            else:
                weights[neighbor] *= 1 + ARGS.learning_rate * reward_scale * rewards[neighbor]

        # normalize weights
        normalizer = 1 / sum(weights.values())
        for neighbor in self.neighbors:
            weights[neighbor] *= normalizer

    def request(self, dst, payload=None):
        seq = self.seq_counter[dst]
        self.seq_counter[dst] += 1

        # create new message and enqueue
        msg = Message(TYPE_PING, seq, self.name, dst, payload=payload)
        key = (msg.seq, msg.src, msg.dst)

        self.ping_log.update({
            key: None
        })
        self.msg_queue.append((msg, None))  # enqueue with unknown to_node_id

    def response(self, seq, to_node_id, src, dst, payload=None):
        msg = Message(TYPE_PONG, seq, src, dst, payload=payload)
        self.msg_queue.append((msg, to_node_id))

    def recv(self, msg, from_node_id):
        if msg.msg_type == TYPE_PING:
            # update weight for source node
            self.update_weights(msg.src, from_node_id, 1 / msg.count)

            key = (msg.seq, msg.src, msg.dst)

            if key in self.ping_log:  # cycling case
                self.response(msg.seq, from_node_id, msg.src, msg.dst, payload=PAYLOAD_FAIL)

            elif msg.dst == self.name:  # final receiver case
                self.response(msg.seq, from_node_id, msg.src, msg.dst, payload=PAYLOAD_SUCCESS)

            else:
                self.ping_log.update({
                    key: from_node_id
                })
                self.msg_queue.append((msg, None))  # enqueue with unknown to_node_id

        elif msg.msg_type == TYPE_PONG:
            # update weights for dest node
            self.update_weights(msg.dst, from_node_id, 1 / msg.count if msg.payload == PAYLOAD_SUCCESS else -1)

            # remove entry from ping log
            key = (msg.seq, msg.src, msg.dst)
            assert key in self.ping_log
            to_node_id = self.ping_log[key]
            del self.ping_log[key]

            # forward message if necessary
            if to_node_id is not None:
                self.msg_queue.append((msg, to_node_id))

        else:
            raise ValueError()

    def send(self):
        if not self.msg_queue:
            return None, None

        msg, to_node_id = self.msg_queue.popleft()
        weights = self.weight_table[msg.dst]
        msg.count += 1

        if to_node_id is None:
            weights = [weights[neighbor] for neighbor in self.neighbors]
            to_node_id = random.choices(self.neighbors, weights=weights)[0]

        return msg, to_node_id


def neighbor_type(arg):
    tokens = arg.split(',')

    if len(tokens) != 2:
        raise argparse.ArgumentTypeError()

    try:
        node_a = int(tokens[0])
        node_b = int(tokens[1])
    except ValueError:
        raise argparse.ArgumentTypeError()

    if node_a < 0 or node_b < 0 or node_a == node_b:
        raise argparse.ArgumentTypeError()

    return (min(node_a, node_b), max(node_a, node_b))


def sigint_handler(signum, frame):
    global TERMINATE
    TERMINATE = True


def main():
    global ARGS
    signal.signal(signal.SIGINT, sigint_handler)

    # parse arguments
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('N_NODES', type=int)
    arg_parser.add_argument('EDGE', nargs='*', type=neighbor_type)
    arg_parser.add_argument('--learning-rate', type=float, default=1)
    arg_parser.add_argument('--reward-decay', type=float, default=0.5)
    arg_parser.add_argument('--num-ticks', type=int)
    arg_parser.add_argument('--log-file')
    arg_parser.add_argument('--brief', action='store_true')
    ARGS = arg_parser.parse_args()

    n_nodes = ARGS.N_NODES
    adjacent_pairs = ARGS.EDGE
    adjacent_lists = defaultdict(list)

    # open log file
    if ARGS.log_file is not None:
        log_file = open(ARGS.log_file, 'w')

    # build graph representation
    for node_a, node_b in adjacent_pairs:
        if node_a >= n_nodes or node_b >= n_nodes:
            raise ValueError()

        adjacent_lists[node_a].append(node_b)
        adjacent_lists[node_b].append(node_a)

    node_list = list()

    for ind in range(n_nodes):
        neighbors = adjacent_lists[ind]
        node_list.append(Node(ind, neighbors))

    # write network description into log
    if ARGS.log_file is not None:
        net_desc = {
            'n_nodes': n_nodes,
            'edges': adjacent_pairs,
        }
        log_file.write(json.dumps(net_desc))
        log_file.write('\n')

    # start simulation
    msg_src = 0
    msg_dst = n_nodes - 1

    for tick in itertools.count():
        # gather sent messages
        buffered_msgs = defaultdict(list)

        for ind, node in enumerate(node_list):
            msg, to_node_id = node.send()

            if msg is not None:
                buffered_msgs[to_node_id].append((msg, ind))

        # check if the network has messages
        if not buffered_msgs:
            node_list[msg_src].request(msg_dst)

        # transfer messages to next hop
        for to_node_id, msgs_of_channel in buffered_msgs.items():
            random.shuffle(msgs_of_channel)

            for msg, from_node_id in msgs_of_channel:
                node_list[to_node_id].recv(msg, from_node_id)

        # save logs
        transfered_messages = list()

        for from_node_id, msgs in buffered_msgs.items():
            for (msg, to_node_id) in msgs:
                msg_desc = {
                    'type': msg.msg_type,
                    'from': from_node_id,
                    'to': to_node_id,
                    'src': msg.src,
                    'dst': msg.dst,
                    'count': msg.count,
                    'payload': msg.payload,
                }
                transfered_messages.append(msg_desc)

        dict_log = {
            'tick': tick,
            'transfered_messages': transfered_messages,
            'weights': {node.name: dict(node.weight_table) for node in node_list},
            'rewards': {node.name: dict(node.reward_table) for node in node_list},
        }


        if ARGS.log_file is not None:
            json_log = json.dumps(dict_log)
            log_file.write(json_log)
            log_file.write('\n')

        if not ARGS.brief:
            pprint.pprint(dict_log)

        if TERMINATE or tick + 1 == ARGS.num_ticks:
            break

    # print summary
    print('= n_nodes: %d' % n_nodes)
    print('= edges: %s' % repr(adjacent_pairs))

    for node_id, node in enumerate(node_list):
        dst_node_ids = list(node.weight_table.keys())
        dst_node_ids.sort()

        header = ['dest\\neighbor'] + node.neighbors

        # build weight table
        weight_gen = (node.weight_table[dst] for dst in dst_node_ids)
        w_table = list()
        for dst, weights in zip(dst_node_ids, weight_gen):
            row = [dst] + list(weights[neighbor] for neighbor in node.neighbors)
            w_table.append(row)

        # build reward table
        reward_gen = (node.reward_table[dst] for dst in dst_node_ids)
        r_table = list()
        for dst, rewards in zip(dst_node_ids, reward_gen):
            row = [dst] + list(rewards[neighbor] for neighbor in node.neighbors)
            r_table.append(row)

        # draw tables
        print('= weights of node %d' % node_id)
        print(tabulate(w_table, header, tablefmt='grid'))
        print()

        print('= rewards of node %d' % node_id)
        print(tabulate(r_table, header, tablefmt='grid'))
        print()


    if ARGS.log_file is not None:
        log_file.close()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('interrupted by user')
